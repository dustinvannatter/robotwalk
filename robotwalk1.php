<?php

	function walk($direction, $instruction, $cur_x, $cur_y, $prev_points) {
		$next_direction = [
			"n" => "e",
			"e" => "s",
			"s" => "w",
			"w" => "n"
		];
		$instructions = [
			"n" => ["axis"=>"y", "inc"=>"+"],
			"e" => ["axis"=>"x", "inc"=>"+"],
			"s" => ["axis"=>"y", "inc"=>"-"],
			"w" => ["axis"=>"x", "inc"=>"-"]
		];

		$cur_direction = $next_direction[$direction];
		while ($instruction--) {
			if ($instructions[$direction]["inc"] == "+") {
				${"cur_".$instructions[$direction]["axis"]}++;
			} else {
				${"cur_".$instructions[$direction]["axis"]}--;
			}

			// if we already have went to this spot, exit..
			if (in_array($cur_x . "," . $cur_y, $prev_points)) {
				return array($cur_x, $cur_y, $prev_points, $cur_direction, 1);
			}
			$prev_points[] = $cur_x . "," . $cur_y;
		}
		return array($cur_x, $cur_y, $prev_points, $cur_direction, 0);
	}

	function robotWalk($instructions) {

		$cur_x = 0;
		$cur_y = 0;
		$prev_points = [];
		$cur_direction = "n";

		foreach ($instructions as $instruction) {

			switch($cur_direction) {

				case "n":
					list ($cur_x, $cur_y, $prev_points, $cur_direction, $do_return) = walk("n", $instruction, $cur_x, $cur_y, $prev_points);
					if ($do_return) { return array($cur_x, $cur_y); }
					break;

				case "e":
					list ($cur_x, $cur_y, $prev_points, $cur_direction, $do_return) = walk("e", $instruction, $cur_x, $cur_y, $prev_points);
					if ($do_return) { return array($cur_x, $cur_y); }
					break;

				case "s":
					list ($cur_x, $cur_y, $prev_points, $cur_direction, $do_return) = walk("s", $instruction, $cur_x, $cur_y, $prev_points);
					if ($do_return) { return array($cur_x, $cur_y); }
					break;

				case "w":
					list ($cur_x, $cur_y, $prev_points, $cur_direction, $do_return) = walk("w", $instruction, $cur_x, $cur_y, $prev_points);
					if ($do_return) { return array($cur_x, $cur_y); }
					break;

			}
		}

		return array($cur_x, $cur_y);
	}

	$results = robotWalk([1, 2, 4]); // should return (2, -3)
	echo "<pre style='background-color:pink;'>";
	print_r($results);
	echo "</pre>";
	echo "<br/>";

	$results = robotWalk([1, 2, 4, 1, 5]); // should return (1, 1)
	echo "<pre style='background-color:mediumpurple;'>";
	print_r($results);
	echo "</pre>";
	echo "<br/>";

?>