<?php

	function robotWalk($instructions) {

		$cur_x = 0;
		$cur_y = 0;
		$prev_points = [];
		$cur_direction = "n";

		$next_direction = [
			"n" => "e",
			"e" => "s",
			"s" => "w",
			"w" => "n"
		];

		foreach ($instructions as $instruction) {

			switch($cur_direction) {
				case "n":
					while ($instruction--) {
						$cur_y++;
						// if we already have went to this spot, exit..
						if (in_array($cur_x . "," . $cur_y, $prev_points)) {
							return array($cur_x, $cur_y);
						}
						$prev_points[] = $cur_x . "," . $cur_y;
					}
					$cur_direction = $next_direction["n"];
					break;

				case "e":
					while ($instruction--) {
						$cur_x++;
						// if we already have went to this spot, exit..
						if (in_array($cur_x . "," . $cur_y, $prev_points)) {
							return array($cur_x, $cur_y);
						}
						$prev_points[] = $cur_x . "," . $cur_y;
					}
					$cur_direction = $next_direction["e"];
					break;

				case "s":
					while ($instruction--) {
						$cur_y--;
						// if we already have went to this spot, exit..
						if (in_array($cur_x . "," . $cur_y, $prev_points)) {
							return array($cur_x, $cur_y);
						}
						$prev_points[] = $cur_x . "," . $cur_y;
					}
					$cur_direction = $next_direction["s"];
					break;

				case "w":
					while ($instruction--) {
						$cur_x--;
						// if we already have went to this spot, exit..
						if (in_array($cur_x . "," . $cur_y, $prev_points)) {
							return array($cur_x, $cur_y);
						}
						$prev_points[] = $cur_x . "," . $cur_y;
					}
					$cur_direction = $next_direction["w"];
					break;
			}

		}

		return array($cur_x, $cur_y);
	}

	$results = robotWalk([1, 2, 4]); // should return (2, -3)
	echo "<pre style='background-color:pink;'>";
	print_r($results);
	echo "</pre>";
	echo "<br/>";

	$results = robotWalk([1, 2, 4, 1, 5]); // should return (1, 1)
	echo "<pre style='background-color:mediumpurple;'>";
	print_r($results);
	echo "</pre>";
	echo "<br/>";

?>